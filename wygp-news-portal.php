<?php
/*
* Plugin Name: wygp-news-portal
* Author: Israel Duff
* Description: The news portal plugin for the WYGP system
* Version: 0.0.0
*/

/*
* Include the rss manager class
*/
require_once(dirname(__FILE__)."/rss.php");
register_activation_hook(__FILE__,array("RSS","plugin_activated"));


/*
* Include the News Manager class
*/
require_once(dirname(__FILE__).'/manager.php');
register_activation_hook(__FILE__,array("NewsManager","plugin_activated"));
?>
