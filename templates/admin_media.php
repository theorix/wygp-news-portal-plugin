<div class="container">
  <h3>File Upload Page</h3>
  <form action="http://localhost/theorix/wp-admin/admin-post.php" name="" method="post" enctype="multipart/form-data">
    <input type="hidden" name="action" value="upload_image">
    <input type="hidden" name="data" value="non">
    <div class="form-group">
      <label for="upload_f" style="background-color:black; text-align:center; color:white; width:20%; padding:2%">Media File</label>
      <input type="file" name="adminpic" id="upload_f" style="opacity:0" multiple>
    </div>
    <div class='form-group'>
      <label for="img_name">Picture Name</label>
      <input type="text" placeholder="Enter picture name (Optional)" name="img_name" id="img_name">
    </div>
    <input type="submit" name="upload_img" value="Upload">
  </form>
</div>
