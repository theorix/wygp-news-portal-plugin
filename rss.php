<?php
/*
* this is the rss manager class
*/

include_once "test_rss.php";

class RSS {
  private $number_of_news_fetch;
  private $item_file = array();
  public function __construct($n, $expire_time_minutes) { //adds shortcodes and hooks
    add_shortcode("wygp-rss-feed",array($this,'getContent'));

    $this->number_of_news_fetch = $n;
    $this->expire_time_minutes = $expire_time_minutes;
  }


  /**
    *             Plugin activation hook
    * This function (or method) is executed when the plugin is activated.
    * It Creates all WordPress pages needed by the plugin.
  */
    public static function plugin_activated() {
      $newsPage = array(
        array(
          "title"=>"News Portal",
          "content"=>"[wygp-rss-feed]",
        )
      );

      //create the post from the pages
      foreach($newsPage as $slug=>$page) {
        $query = new WP_Query("pagename=".$slug);
        if(!$query->has_posts()) {
          wp_insert_post(array(
            "post_name"=>$slug,
            "post_title"=>$page['title'],
            "post_content"=>$page['content'],
            "post_type"=>'page',
            "post_status"=>"publish",
            "ping_status"=>"closed"
          ));
        }
      }
    }

/*
* Method used to get news feed given the url of the feed.
* It basically fetches (or loads) the (xml) content of the url
*/
function getFeed($url){
    $rss = simplexml_load_file($url); //fetch the xml data from url
    $count = 0;
     //print_r($rss);
    $image = $rss->channel->image->url;
    $html = '<ul>'."<img alt='no image' src='$image'/>";
    foreach($rss->channel->item as $item) {
        $count++;
        if($count > $this->number_of_news_fetch){
            break;
        }
        echo $item->link;
        $html .= '<li><a href="'.htmlspecialchars($item->link).'">'.htmlspecialchars($item->title).'</a></li>';
    }
    $html .= '</ul>';
    return $html;
}

public function get_rss_file($rss_url) {
  $fp = fopen($rss_url,'r');
  $html = '';
  if($fp) {
    while(($buffer=fgets($fp,4096)) !== false) {
      $html .= $buffer;
    }
    if(!feof($fp)) {
      echo "Fget faild to load url";
    }
    fclose($fp);
    file_put_contents(dirname(__FILE__)."/news-portal-files/raw-feed-cache.html",$html);
    //return $html;
    //return dirname(__FILE__)."/news-portal-files/raw-feed-cache.html";
    echo $rss_url;
    return $rss_url;
  }
}


/*
* Method used to get fresh content from the different rss feed source
*/
  public function getFreshContent() { //NOTE: the number of rss feed sources is directly proportional to site load time
      $html = "";
      $newsSource = array(
          array(
              "title" => "BBC",
              "url" => "http://feeds.bbci.co.uk/news/world/rss.xml"
          ),
          array(
              "title" => "CNN",
              "url" => "http://rss.cnn.com/rss/cnn_latest.rss"
          ),
          array(
              "title" => "Fox News",
              "url" => "http://feeds.foxnews.com/foxnews/latest"
          ),
          array(
            "title"=>"News 2",
            "url"=>"http://thepoliticalinsider.com/feed"
          ),
          
          array("title"=>"New 4",
          "url"=>"http://hotair.com/feed"
          )
      );

      foreach($newsSource as $source) {
          $html .= '<h2>'.$source["title"].'</h2>';
          //$html .= $this->getFeed($source["url"]);
          $parse = new RSSParser($source['url']);
          $html .= $parse->getOutput();
      }
      return $html;
  }

  public function get_expire_time() {
    return $this->expire_time_minutes * 60;
  }


  /*
  * Method to provide cached content for the different rss feed sources
  */
  public function getContent() {
   //Thanks to https://davidwalsh.name/php-cache-function for cache idea
      $file = dirname(__FILE__)."/news-portal-files/feed-cache.txt";
      //$fd = fopen($file,'w+');
      $current_time = time();
      $expire_time = $this->get_expire_time();
      $file_time = filemtime($file);
      if(file_exists($file) && ($current_time - $expire_time < $file_time)) {
          return file_get_contents($file);
      }
      else {
          $content = @$this->getFreshContent();
          file_put_contents($file, $content);
          return $content;
      }

  return $output;
}

}

$rss = new RSS(7,1);



?>
