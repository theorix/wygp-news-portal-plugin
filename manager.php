<?php

//include_once "functions.php";

class NewsManager {

  public function __construct() {
    add_action("init",array($this,"create_post_type"));
    add_shortcode("wygp-news-page",array($this, "render_news_form"));
    add_shortcode("wygp-admin-upload-page",array($this,"render_media_form"));
    add_action("admin_post_add_news",array($this,"create"));
    add_action("admin_post_upload_image",array($this,"attachment"));
    add_filter('post_type_link', array($this,'newscategory_permalink_structure'), 10, 4);
  }


  public function plugin_activated() {

        global $wpdb;

    //use the wp database prefix defined
     $table_name = $wpdb->prefix."wygp_media_news";

     //get charset collate
     $char_set_collate = $wpdb->get_charset_collate();

     //write your sql query to create table
     $query = "CREATE TABLE IF NOT EXISTS $table_name (
       id mediumint(9) NOT NULL AUTO_INCREMENT,
       username varchar(55) DEFAULT '' NOT NULL,
       file_url varchar(250) DEFAULT '' NOT NULL,
       PRIMARY KEY  (id)
     ) $char_set_collate;";


     //execute the query using the db delta function
     require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
     dbDelta( $query );

     //define the news portal pages
    $news_admin_page = array(
      array(
        "title"=>"News Page",
        "content"=>"[wygp-news-page]"
      ),
      array(
        "title"=>"Media Upload",
        "content"=>"[wygp-admin-upload-page]"
      )
    );

    foreach($news_admin_page as $slug=>$page) {
      //use wordpress query class
      $query = new WP_Query("pagename=".$slug);
      if(!$query->has_posts()) {
        wp_insert_post(array(
          "post_title"=>$page['title'],
          "post_name"=>$slug,
          "post_type"=>"page",
          "post_content"=>$page['content'],
          "post_status"=>"publish",
          "ping_status"=>"closed",
        ));
      }
    }

    //call the custom post type method here
    NewsManager::create_post_type();
    //flush the rewrite here
    flush_rewrite_rules(false);
  }

  public function create() {
    //handle form here
    status_header(200);

    //get the content from the news page
    $news_id = $_REQUEST['data'];
    $news_data = $_REQUEST['myId'];
    $news_title = $_REQUEST['news-title'];
    $cat = $_REQUEST['news-cat']=="select"?"general":$_REQUEST['news-cat'];
    $cat_id = get_category_by_slug("wygp-news");
    $author = (wp_get_current_user())->user_login;
    //$cat_id = $cat_id->cat_ID;
    //print_r($cat_id);
    //use the above received content to add a new (news) post via wp_insert_post()
    $post_id = wp_insert_post(array(
      "post_content"=>$news_data,
      "post_title"=>$news_title,
      //"post_name"=>$news_id,
      "post_status"=>"publish",
      "post_type"=>"wygp_news",
      "post_author"=>$author,
      //"post_category"=>array($cat_id->term_id),
      "ping_status"=>"closed",
      "comment_status"=>"close"
    )); //returns int | WP_Error

    //add a category to the post
    wp_set_object_terms($post_id,$cat,'newscat');

    //print_r($status);

    //when done, redirect the user back to the editor with a confirmation msg
    $command = "done";
    $msg = urlencode($command);
    wp_redirect("http://localhost/theorix/news-page?command=$msg");

    //NOTE: All request handlers should die after they have completed their task
    die("The server received '{$news_data}' from the browser");

  }

  public function render_news_form() {
    //$this->attachment();
    return $page_tmpl = $this->get_template_html("admin_page");
  }

  public function render_media_form() {
    return $this->get_template_html("admin_media");
  }


  function get_template_html( $template_name, $attributes = null ) {
      if ( ! $attributes ) {
          $attributes = array();
      }

      ob_start();   //i.e start recording...
      require( 'templates/' . $template_name . '.php'); // include the file -- (its now in buffer)
      $html = ob_get_contents();      //get the contents of the file in buffer
      ob_end_clean(); // since done copying from buffer, delete the contents

      return $html;
  }

  function attachment() {
    //$file = dirname(__FILE__).'/news-portal-files/israel.jpg';
    $file = $_FILES['adminpic']['name'];
    $file_tmp_name = $_FILES['adminpic']['tmp_name'];
    $filename = basename($file);
    $parent_post_id = 4;
    $upload_file = wp_upload_bits($filename, null, file_get_contents($file_tmp_name));
    if (!$upload_file['error']) {
    	$wp_filetype = wp_check_filetype($filename, null );
      $wp_file_url = $upload_file['url'];

      /*
      * There are several things you can do to this uploaded file info
      *  1. Store the file info to db against the user id of this file for later retrival based on owner
      *  2. Send the file url back to the admin media page for use
      */

      //Doing number 1
      global $wpdb;
      //use the wp database prefix defined
       $table_name = $wpdb->prefix."wygp_media_news";
       $user = wp_get_current_user();

       $wpdb->insert($table_name,array(
         "username"=>$user->user_login,
         "file_url"=>$wp_file_url
       ));

      //this commented lines of will add the uploaded media to the wp medial lib
    	/*$attachment = array(
    		'post_mime_type' => $wp_filetype['type'],
    		'post_parent' => $parent_post_id,
    		'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
    		'post_content' => '',
    		'post_status' => 'inherit'
    	);
    	$attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $parent_post_id );
    	if (!is_wp_error($attachment_id)) {
    		require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    		$attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
    		wp_update_attachment_metadata( $attachment_id,  $attachment_data );
    	} */
    }

    die("Your file url is".$wp_file_url);
  }

  //create a custom news type
  public static function create_post_type() {
    //first, create the custom post type
    register_post_type(
      'news',
      array(
        'labels'=>array(
          "name"=>__("News"),
          "singular_name"=>__("News")
        ),
        "public"=>true,
        "has_archive"=>true,
        'taxonomies'=>array('newstype'),
        'rewrite'=>array("slug"=>"news/%newscat%","with_front"=>false)
      )
    );

    //second, create a custom taxonomy for the custom type created
    register_taxonomy(
      'newscat',
      "news",
      array(
        'label'=>__("WYGPNewsCategories"),
        "rewrite"=>array("slug"=>"news"),
        "capabilities"=>array(
          'assign_terms'=>'edit_guides',
          'edit_terms'=>'publish_guides'
        )
      )
    );
  }

//deal with the permalink structure
public function newscategory_permalink_structure($post_link, $post, $leavename, $sample) {
    if (false !== strpos($post_link, '%newscat%')) {
        $newscategory_type_term = get_the_terms($post->ID, 'newscat');
        if (!empty($newscategory_type_term))
            $post_link = str_replace('%newscat%', array_pop($newscategory_type_term)->slug, $post_link);
        else
            $post_link = str_replace('%newscat%', 'uncategorized', $post_link);
    }
    return $post_link;
}

  //function to add/remove taxonomies (news categories)
  function manage_cat($action) {

  }
}

$newsManager = new NewsManager();
?>
